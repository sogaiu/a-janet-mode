;;; a-janet-ts.el --- tree sitter -*- lexical-binding: t; -*-

;; Author: sogaiu
;; Version: 0.1.0
;; URL: https://codeberg.org/sogaiu/a-janet-mode
;; Package-Requires: ((emacs "25.2") (tree-sitter "0.13.0"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Acknowledgments:

;; emacs-tree-sitter

;;; Code:

(require 'tree-sitter)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; XXX: modification of tree-sitter-node-at-point
(defun a-janet-ts-node-at-pos (&optional position node-type)
  "Return the smallest syntax node at POSITION with type NODE-TYPE.
If NODE-TYPE is nil, return the smallest syntax node at point."
  (let* ((root (tsc-root-node tree-sitter-tree))
         (p (or position (point)))
         (node (tsc-get-descendant-for-position-range root p p)))
    (if node-type
        (let ((this node) result)
          (while this
            (if (equal node-type (tsc-node-type this))
                (setq result this
                      this nil)
              (setq this (tsc-get-parent this))))
          result)
      node)))

(defun a-janet-ts-named-node-at-pos (&optional position node-type)
  "Return the smallest syntax node at POSITION with type NODE-TYPE.
If NODE-TYPE is nil, return the smallest syntax node at point."
  (let* ((root (tsc-root-node tree-sitter-tree))
         (p (or position (point)))
         (node (tsc-get-named-descendant-for-position-range root p p)))
    (if node-type
        (let ((this node) result)
          (while this
            (if (equal node-type (tsc-node-type this))
                (setq result this
                      this nil)
              (setq this (tsc-get-parent this))))
          result)
      node)))

;; XXX: docstring is opaque, so here is some background
;;
;;      the point of the idea of a "context node" is to be able to
;;      ask what the "containing" node is for a position.  for
;;      example, suppose there is the following form:
;;
;;        (a b)
;;
;;      the "context" node for `a` is the node for the tuple that
;;      spans from the opening paren to the closing paren.
;;
;;      we also want the "context" node for ` ` (between `a` and
;;      `b`) to be the same tuple-spanning node.
;;
;;      however, the smallest syntax node containing `a` is a symbol
;;      node, while that for ` ` is the tuple node.
;;
;;      in the janet-simple grammar, whitespace (that is not part of a
;;      string, say) is handled as a member of "extras".  this means
;;      the whitespace doesn't get its own node (like say, a "symbol"
;;      might), but rather it is considered part of some other node.
;;
;;      when tree sitter is queried about the smallest node that
;;      contains such a whitespace character, it typically will give
;;      an answer about the "context" node, e.g. a tuple, a table,
;;      etc.
;;
;;      in contrast, when tree sitter is queried about the smallest
;;      node that contains a non-whitespace character (or a whitespace
;;      character in the middle of a string, for example), it will
;;      give an answer that isn't about the "context" node.
;;
;;      so the point is that it doesn't make sense to always just look
;;      for the parent of the smallest syntax node for a position.  it
;;      depends on the nature of the character at the position.
(defun a-janet-ts-context-node-at-pos (&optional position node-type)
  "Return context node at POSITION with type NODE-TYPE.
If NODE-TYPE is nil, return the context of the smallest syntax
node at point."
  ;; XXX: need to check whether successful?
  (let* ((node (a-janet-ts-node-at-pos position node-type))
         (the-type (tsc-node-type node)))
    (cond ((memq the-type '(par_tup_lit par_arr_lit
                            sqr_tup_lit sqr_arr_lit
                            struct_lit tbl_lit
                            source))
           node)
          ;; `(tsc-get-parent node)' != nil, since `the-type' != 'source
          ((member the-type '("(" "@("
                              "[" "@["
                              "{" "@{"))
           ;; will not be nil, at worst 'source
           (tsc-get-parent (tsc-get-parent node)))
          ;; XXX: handled in last case
          ;;((member the-type '(")"
          ;;                    "]"
          ;;                    "}"))
          ;; (tsc-get-parent node))
          ((let* ((parent-node (tsc-get-parent node))
                  (parent-type (tsc-node-type parent-node)))
             (memq parent-type '(quasi_quote_form quote_form
                                 short_fn_form splice_form
                                 unquote_form)))
           ;; will not be nil, at worst 'source
           (tsc-get-parent (tsc-get-parent node)))
          (the-type
           (tsc-get-parent node)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun a-janet-column-number-at-pos (position)
  "Return column number for POSITION."
  (save-excursion
    (goto-char position)
    (current-column)))

(defun a-janet-open-delimiter-col (node)
  "Compute the column of the beginning of the opening delimiter for NODE."
  (let ((start-of-delim (tsc-node-start-position node)))
    (a-janet-column-number-at-pos start-of-delim)))

(defun a-janet-count-previous-child-nodes (ctx-node line-number)
  "Count CTX-NODE's child nodes that occur before the line LINE-NUMBER."
  (let* ((index 0)
         (cur-node (tsc-get-nth-named-child ctx-node index))
         (done nil))
    (while (and cur-node (not done))
      (let ((node-line-number (line-number-at-pos
                               (tsc-node-start-position cur-node))))
        (if (>= node-line-number line-number)
            (setq done t)
          (setq index (1+ index))
          (setq cur-node (tsc-get-next-sibling cur-node)))))
    (if done
        index ; turns out that index is the same as count in this case
      nil)))

(provide 'a-janet-ts)
;;; a-janet-ts.el ends here
