= a-janet-mode - an emacs-tree-sitter-based janet-mode
:toc:

== Status

In progress, but usable.

== Prerequisites

* Emacs with dynamic module support
* emacs-tree-sitter

== Setup

=== Emacs with dynamic module support

Many distributions do not ship Emacs with dynamic module support
enabled (yet?).  It may be necessary to compile Emacs from source
appropriately (e.g. use `--with-modules` with `configure`) or find an
appropriately compiled version.

On macos, some alternative tap might work,
e.g. https://github.com/d12frosted/homebrew-emacs-plus[homebrew-emacs-plus]

=== emacs-tree-sitter

Follow the https://ubolonton.github.io/emacs-tree-sitter/installation/[official instructions].

Alternatively, if using straight.el, put something like the following
in your `.emacs` equivalent:

[source,janet]
----
;;; emacs-tree-sitter
(straight-use-package 'tree-sitter)
(straight-use-package 'tree-sitter-langs)

(require 'tree-sitter)
(require 'tree-sitter-langs)
----

=== a-janet-mode

Put something like the following in your `.emacs` equivalent.

straight.el users:

[source,janet]
----
;;; a-janet
(straight-use-package
 '(a-janet-mode
   :type git
   :repo "https://codeberg.org/sogaiu/a-janet-mode.git"
   :branch "main"
   :files ("*.el" "*.janet" "highlights")))

(use-package a-janet-mode
  :straight t)
----

Other users:

```
(add-to-list 'load-path
             "path-to-a-janet-mode-cloned-repository")
(require 'a-janet-mode)
```

== Usage

If all went well, opening a `.janet` file should yield a syntax
highlighted result.

Indentation is much more likely to work properly if one's source is
"well-formed", at least with respect to delimiters.  The simplest way
to achieve this in Emacs may be: `M-x electric-pair-mode`.  However,
`smartparens` and other things may work too.

== Technical Notes

The notes.txt file contains a prose description and notes for the
algorithm used for indentation.  Sorry there's no link, I didn't
figure out how to get that working here.

== Acknowledgments

Thanks to:

* andrewchambers
* bakpakin
* crocket
* GrayJack
* pepe
* pyrmont
* Saikyun
* uvtc

...and other Janet community members :)
