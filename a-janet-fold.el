;;; a-janet-fold.el --- folding -*- lexical-binding: t; -*-

;; Author: sogaiu
;; Version: 0.1.0
;; URL: https://codeberg.org/sogaiu/a-janet-mode
;; Package-Requires: ((emacs "25.2") (tree-sitter "0.13.0"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Acknowledgments:

;; hideshow.el

;;; Code:

(require 'tree-sitter)

(defvar-local a-janet-comment-block-folds nil)

(defun a-janet-unfold-comment-blocks ()
  "Unfold top level comment blocks."
  (interactive)
  (seq-doseq (ov a-janet-comment-block-folds)
    (delete-overlay ov))
  (setq a-janet-comment-block-folds nil))

(defun a-janet-fold-comment-blocks ()
  "Fold top level comment blocks."
  (interactive)
  (let* ((root (tsc-root-node tree-sitter-tree))
         (index 0)
         (cur-node (tsc-get-nth-named-child root index)))
    (a-janet-unfold-comment-blocks)
    (setq a-janet-comment-block-folds ())
    (while cur-node
      (let ((node-type (tsc-node-type cur-node)))
        (when (and (eq 'par_tup_lit node-type)
                   (< 0 (tsc-count-named-children cur-node)))
          (let ((head-node (tsc-get-nth-named-child cur-node 0)))
            (when (and (eq 'sym_lit
                           (tsc-node-type head-node))
                       (string= "comment"
                            (tsc-node-text head-node)))
              (let ((ov (make-overlay (tsc-node-end-position head-node)
                                      (tsc-node-end-position cur-node))))
                (overlay-put ov 'invisible t)
                (overlay-put ov 'after-string "...)")
                (setq a-janet-comment-block-folds
                      (cons ov a-janet-comment-block-folds)))))))
      (setq index (1+ index))
      (setq cur-node (tsc-get-nth-named-child root index)))))

(defun a-janet-toggle-comment-blocks ()
  "Toggle folding of comment blocks."
  (interactive)
  (if a-janet-comment-block-folds
      (a-janet-unfold-comment-blocks)
    (a-janet-fold-comment-blocks)))

(defvar-local a-janet-long-string-folds nil)

(defun a-janet-unfold-long-strings ()
  "Unfold long strings."
  (interactive)
  (seq-doseq (ov a-janet-long-string-folds)
    (delete-overlay ov))
  (setq a-janet-long-string-folds nil))

(defun a-janet-fold-long-strings ()
  "Fold long strings."
  (interactive)
  (let* ((root (tsc-root-node tree-sitter-tree))
         (index 0)
         (cur-node (tsc-get-nth-named-child root index)))
    (a-janet-unfold-long-strings)
    (setq a-janet-long-string-folds ())
    (while cur-node
      (let ((node-type (tsc-node-type cur-node)))
        (when (and (eq 'par_tup_lit node-type)
                   (< 2 (tsc-count-named-children cur-node)))
          (let ((third-node (tsc-get-nth-named-child cur-node 2)))
            (when (eq 'long_str_lit (tsc-node-type third-node))
              (let ((ov (make-overlay (1+ (tsc-node-start-position third-node))
                                      (tsc-node-end-position third-node))))
                (overlay-put ov 'invisible t)
                (overlay-put ov 'after-string "...`")
                (setq a-janet-long-string-folds
                      (cons ov a-janet-long-string-folds)))))))
      (setq index (1+ index))
      (setq cur-node (tsc-get-nth-named-child root index)))))

(defun a-janet-toggle-long-strings ()
  "Toggle folding of long strings."
  (interactive)
  (if a-janet-long-string-folds
      (a-janet-unfold-long-strings)
    (a-janet-fold-long-strings)))

(provide 'a-janet-fold)
;;; a-janet-fold.el ends here
