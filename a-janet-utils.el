;;; a-janet-utils.el --- utils for a-janet-mode -*- lexical-binding: t; -*-

;; Author: sogaiu
;; Version: 0.1.0
;; URL: https://codeberg.org/sogaiu/a-janet-mode
;; Package-Requires: ((emacs "25.2") (tree-sitter "0.13.0"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;; XXX: hack for helping `a-janet-package-path'
(defvar a-janet-utils-thing nil)

;; XXX: may be there's a better way to do this
(defun a-janet-package-path ()
  "Determine path to containing directory of a-janet package."
  (locate-dominating-file (symbol-file 'a-janet-utils-thing)
                          "a-janet-mode.el"))

(provide 'a-janet-utils)
;;; a-janet-util.el ends here
