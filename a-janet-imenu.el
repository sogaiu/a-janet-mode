;;; a-janet-imenu.el --- imenu -*- lexical-binding: t; -*-

;; Author: sogaiu
;; Version: 0.1.0
;; URL: https://codeberg.org/sogaiu/a-janet-mode
;; Package-Requires: ((emacs "25.2") (tree-sitter "0.13.0"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Acknowledgments:

;; imenu

;;; Code:

(require 'tree-sitter)

;; XXX: picks up things in comment blocks too...
;; (defvar a-janet-def-pattern
;;   (rx (syntax open-parenthesis)
;;       (or "def" "def-" "defn" "defn-" "varfn" "varfn-") (1+ space)
;;       (group (1+ (or (syntax word) (syntax symbol) "-" "_")))))

(defun a-janet-create-imenu-index ()
  "Return an imenu index alist for the curret buffer."
  (let* ((root (tsc-root-node tree-sitter-tree))
         (index 0)
         (cur-node (tsc-get-nth-named-child root index))
         (index-alist ()))
    (while cur-node
      (let ((node-type (tsc-node-type cur-node)))
        (when (and (eq 'par_tup_lit node-type)
                   (< 2 (tsc-count-named-children cur-node)))
          (let* ((head-node (tsc-get-nth-named-child cur-node 0))
                 (name-node (tsc-get-nth-named-child cur-node 1)))
            (when (and (eq 'sym_lit
                           (tsc-node-type head-node))
                       (string-match (rx bos (or "def" "var"))
                                     (tsc-node-text head-node)))
              (setq index-alist
                    (cons (cons (tsc-node-text name-node)
                                (tsc-node-start-position name-node))
                          index-alist))))))
      (setq index (1+ index))
      (setq cur-node (tsc-get-nth-named-child root index)))
    index-alist))

(provide 'a-janet-imenu)
;;; a-janet-imenu.el ends here
