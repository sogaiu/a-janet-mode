;;; a-janet-indent.el --- indentation -*- lexical-binding: t; -*-

;; Author: sogaiu
;; Version: 0.1.0
;; URL: https://codeberg.org/sogaiu/a-janet-mode
;; Package-Requires: ((emacs "25.2") (tree-sitter "0.13.0"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Acknowledgments:

;;; Code:

(require 'a-janet-ts)
(require 'tree-sitter)

;; XXX: it's possible there are more cases where a line should not be
;;      indented.  currently only one case has been identified.
;;      if more come up, perhaps they can be added here.
(defun a-janet-indent-line-p (&optional position)
  "Determine whether current line should be indented.
If POSITION is provided, use the line it is associated with instead of
the current line."
  (save-excursion
    (when position
      (goto-char position))
    (let* ((start-pos (progn
                        (beginning-of-line)
                        (point)))
           (node (a-janet-ts-node-at-pos start-pos))
           (the-type (tsc-node-type node)))
      (if (not (memq the-type '(str_lit long_str_lit)))
          t
        (eql (line-number-at-pos start-pos)
             (line-number-at-pos (tsc-node-start-position node)))))))

(defun a-janet-indent-special-p (ctx-node)
  "Determine if CTX-NODE should be special-case-indented."
  ;; XXX: what if there are no child nodes?
  (let* ((head-node (tsc-get-nth-named-child ctx-node 0))
         (head-text (tsc-node-text head-node)))
    (or (member head-text
                '("case" "comment" "cond" "coro"
                  "def" "def-" "default" "defer" "defglobal"
                  "defmacro" "defmacro-" "defn" "defn-" "do"
                  "each" "eachk" "eachp" "eachy" "edefer"
                  "fn" "for" "forever" "forv"
                  "generate"
                  "if" "if-let" "if-not" "if-with"
                  "label" "let" "loop"
                  "match"
                  "prompt"
                  "repeat"
                  "seq" "short-fn"
                  "try"
                  "unless"
                  "var" "var-" "varfn" "varglobal"
                  "when" "when-let" "when-with" "while"
                  "with" "with-dyns" "with-syms" "with-vars"))
        (string-match (rx bos (or "def" "if-" "when-" "with-"))
                      head-text))))

(defun a-janet-calc-indent-for-special (ctx-node line-number)
  "Return indentation for special-case-indented of CTX-NODE given LINE-NUMBER."
  (let* ((count (a-janet-count-previous-child-nodes ctx-node line-number))
         (delim-col (a-janet-open-delimiter-col ctx-node)))
    (if (= 0 count)
        (1+ delim-col)
      (+ 2 delim-col))))

(defun a-janet-calc-indent-for-funcall (ctx-node line-number)
  "Return indentation for vanilla funcalls for CTX-NODE given LINE-NUMBER."
  (let* ((count (a-janet-count-previous-child-nodes ctx-node line-number))
         (delim-col (a-janet-open-delimiter-col ctx-node)))
    (cond ((= 0 count)
           (1+ delim-col))
          ((= 1 count)
           (+ 2 delim-col))
          (t
           (a-janet-column-number-at-pos
            (tsc-node-start-position
             (tsc-get-nth-named-child ctx-node 1)))))))

(defun a-janet-calculate-indent ()
  "Calculate indentation for the current line as Janet code."
  (when (a-janet-indent-line-p)
    (save-excursion
      (beginning-of-line)
      (skip-chars-forward " \t")
      (let ((ctx-node (a-janet-ts-context-node-at-pos)))
        (unless ctx-node
          (error (concat "Tree-sitter parsing failed..."
                         "may be a missing delimiter?")))
        (let ((the-type (tsc-node-type ctx-node)))
          (cond ((eq the-type 'ERROR)
                 (error (concat "Tree-sitter parsing failed..."
                                "may be a missing delimiter?")))
                ((eq the-type 'source)
                 0)
                ((memq the-type '(sqr_tup_lit
                                  struct_lit))
                 (1+ (a-janet-open-delimiter-col ctx-node)))
                ((memq the-type '(par_arr_lit
                                  sqr_arr_lit
                                  tbl_lit))
                 ;; leading @ adds an extra column
                 (1+ (1+ (a-janet-open-delimiter-col ctx-node))))
                ((eq the-type 'par_tup_lit)
                 (if (zerop (tsc-count-named-children ctx-node))
                     (1+ (a-janet-open-delimiter-col ctx-node))
                   (let ((line-no (line-number-at-pos)))
                     (if (a-janet-indent-special-p ctx-node)
                         (a-janet-calc-indent-for-special ctx-node line-no)
                       (a-janet-calc-indent-for-funcall ctx-node line-no)))))
                (t
                 (error "Unexpected node type: %s" the-type))))))))

(defun a-janet-indent-line ()
  "Indent current line as Janet code."
  (interactive)
  (when-let ((n-cols (a-janet-calculate-indent)))
    (let ((pos (- (point-max) (point))))
      (beginning-of-line)
      (let ((beg (point)))
        (skip-chars-forward " \t")
        (unless (= n-cols (current-column))
          (delete-region beg (point))
          (indent-to n-cols))
        (when (< (point) (- (point-max) pos))
          (goto-char (- (point-max) pos)))))))

(provide 'a-janet-indent)
;;; a-janet-indent.el ends here
