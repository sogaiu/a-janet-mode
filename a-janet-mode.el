;;; a-janet-mode.el --- A major mode for Janet -*- lexical-binding: t; -*-

;; Author: sogaiu
;; Version: 0.1.0
;; URL: https://codeberg.org/sogaiu/a-janet-mode
;; Package-Requires: ((emacs "25.2") (tree-sitter "0.13.0"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Defines a major mode for the janet language: https://janet-lang.org/
;; using tree-sitter.

;; Some Features:
;;
;; * Indentation
;; * Highlighting
;; * Folding (e.g. collapsing strings and comment blocks to reduce clutter)
;; * Code Navigation via imenu

;; Indentation is based on using tree-sitter with the grammar
;; tree-sitter-janet-simple.
;;
;; This works much better if the source being edited is "well-formed",
;; e.g. not missing closing delimiters.  There are a number of ways to
;; address this:
;;
;; 1) Manually
;; 2) Activate Emacs' built-in electric-pair-mode / electric-pair-local-mode
;; 3) Install and activate smartparens
;; 4) Using some other package

;;; Acknowledgments:

;; emacs-tree-sitter
;; janet-mode
;; racket-mode

;;; Code:

(require 'elec-pair) ; XXX: comes with emacs
(require 'a-janet-indent)
(require 'a-janet-fold)
(require 'a-janet-imenu)
(require 'a-janet-utils)
(require 'tree-sitter)
(require 'tree-sitter-hl)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar a-janet-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; comments
    (modify-syntax-entry ?# "<" table)
    (modify-syntax-entry ?\n ">" table)
    ;; keywords
    (modify-syntax-entry ?: "_" table)
    ;; long-string delimiter
    ;; XXX: not sure whether this should be done
    (modify-syntax-entry ?` "\"" table)
    ;; other symbol chars
    (modify-syntax-entry ?? "_" table)
    (modify-syntax-entry ?! "_" table)
    (modify-syntax-entry ?. "_" table)
    (modify-syntax-entry ?@ "_" table)
    ;;
    table))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; XXX: . breaks things...
;; XXX: https://github.com/ubolonton/emacs-tree-sitter/issues/99
;; (defvar-local a-janet-tree-sitter-patterns
;;   []
;;   "Default patterns for tree-sitter support.")

(defvar a-janet-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "\C-c\C-t\C-c" 'a-janet-toggle-comment-blocks)
    (define-key map "\C-c\C-t\C-d" 'a-janet-toggle-long-strings)
    (easy-menu-define a-janet-mode-map map
      "A Janet Mode Menu"
      '("AJanet"
        ["Toggle Comment Blocks" a-janet-toggle-comment-blocks t]
        ["Toggle Long String Docstrings" a-janet-toggle-long-strings t]))
    map)
  "A Janet mode map.")

;;;###autoload
(define-derived-mode a-janet-mode prog-mode "a-janet"
  "Major mode for the Janet language.

\\{a-janet-mode-map}"
  :syntax-table a-janet-mode-syntax-table
  (setq-local indent-line-function #'a-janet-indent-line)
  ;; XXX: provide way to turn this off to use other things like
  ;;      smartparens?
  (electric-pair-local-mode)
  ;; XXX: what are the consequences of not having this?
  ;;(setq-local lisp-indent-function #'a-janet-indent-function)
  (setq-local comment-start "#")
  (setq-local comment-start-skip "#+ *")
  (setq-local comment-use-syntax t)
  (setq-local comment-end "")
  ;;(add-to-list 'imenu-generic-expression `(nil ,a-janet-def-pattern 1))
  (setq-local imenu-create-index-function #'a-janet-create-imenu-index)
  ;;
  ;; tree-sitter stuff
  ;; https://github.com/ubolonton/emacs-tree-sitter/issues/84
  (unless font-lock-defaults
    (setq font-lock-defaults '(nil)))
  (setq-local tree-sitter-hl-default-patterns
              (condition-case nil
                  (with-temp-buffer
                    (insert-file-contents
                     (concat
                      (a-janet-package-path)
                      ;; XXX: will this path work on windows?
                      "/highlights/default.scm"))
                    (goto-char (point-max))
                    (insert "\n")
                    (buffer-string))
                (file-missing nil)))
  ;; XXX: see definition of a-janet-tree-sitter-patterns for why this is
  ;;      commented out atm
  ;;(setq-local tree-sitter-hl-default-patterns
  ;;            a-janet-tree-sitter-patterns)
  ;;
  (setq-local tree-sitter-language
              (tree-sitter-require 'janet-simple))
  (tree-sitter-hl-mode))

;;;###autoload
(progn
  (add-to-list 'auto-mode-alist
               '("\\.janet\\'" . a-janet-mode))
  (add-to-list 'interpreter-mode-alist
               '("janet" . a-janet-mode))
  (add-hook 'a-janet-mode-hook
            'imenu-add-menubar-index))

(provide 'a-janet-mode)
;;; a-janet-mode.el ends here
